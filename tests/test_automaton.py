import unittest
from automaton import *


class Testautomaton(unittest.TestCase):
    def setUp(self):
        # vars        
        self.rowSize = 15
        self.colSize = 20
        self.states = ["Void", "Tree"]
        self.treeStates = ["FIRE", "Tree"]
        self.evolution = {
            "Void": "Void",
            "FIRE": "Ash",
            "Ash": "Void",
            # for the record
            "Tree": "FIRE if a neighbour is on FIRE, else Tree"
        }
        # means 0.7 of having void
        self.initTerrain = init(self.states, self.rowSize, self.colSize, 0.3)
        terrain = cloneLL(self.initTerrain)
        self.fireTerrain = fireCells(terrain)
        self.isFireProba = False

    def test_init(self):
        terrain = self.initTerrain
        self.assertEqual(len(terrain), 15)
        for rows in terrain:
            self.assertEqual(len(rows), self.colSize)
            for tile in rows:
                self.assertTrue(tile in self.states)

    def test_fireCells(self):
        fireStates = self.states
        fireStates.append("FIRE")
        fireTerrain = fireCells(self.initTerrain)
        for rows in fireTerrain:
            for tile in rows:
                self.assertTrue(tile in fireStates)

    def test_launch(self):
        evolutionTerrains = launch(
            self.fireTerrain, self.treeStates, self.evolution,self.isFireProba)
        self.assertTrue(len(evolutionTerrains) >= 1)
        terrain=self.initTerrain
        terrain[1][1]="FIRE"
        terrain[0][1]="Tree"
        terrain[1][0]="Tree"
        terrain[2][1]="Tree"
        terrain[1][2]="Tree"
        evolutionTerrains=launch(terrain, self.treeStates, self.evolution,False)
        self.assertEqual(evolutionTerrains[0][0][1],"FIRE")
        self.assertEqual(evolutionTerrains[0][1][0],"FIRE")
        self.assertEqual(evolutionTerrains[0][2][1],"FIRE")
        self.assertEqual(evolutionTerrains[0][1][2],"FIRE")


    def test_allInchanged(self):
        terrain = self.initTerrain
        self.assertTrue(allInchanged(terrain))
        terrain[0][0] = "FIRE"
        self.assertFalse(allInchanged(terrain))

    def test_evolutionTree(self):
        terrain=evolutionTree(self.initTerrain,self.treeStates,0,0,False)
        for rows in terrain:
            for tile in rows:
                self.assertTrue(tile in self.evolution)
    
    def test_probaFire(self):
        self.assertEqual(probaFire(0),0)
        self.assertEqual(probaFire(1),0.5)
        self.assertEqual(probaFire(4),0.8)

