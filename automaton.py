"""
==============================================================================
Cellular Automaton for Forests Fires
==============================================================================
Author : MONT Louis

Notice that the Fire state is in Caps: FIRE
"""

import random

# "global" variables
rowSize = 15
colSize = 15
initialStates = ["Void", "Tree"]
treeStates = ["FIRE", "Tree"]
evolution = {
    "Void": "Void",
    "FIRE": "Ash",
    "Ash": "Ash",
    # for the record
    "Tree": "Tree"  # FIRE if a neighbour is on FIRE, else Tree
}
voidLuck = 0.5
isFireProba = False
caseSize = 40


def init(states, rowSize, colSize, treeLuck):
    """
    Initialization of the terrain

    For each tile choose between the states provided with a certain luck    

    Parameters:
        states(list(string)): The differents states the tile can have when its start
        rowSize(int): Size Horizontically 
        colSize(int): Size Vertically
        treeLuck(double): the percentage of luck for a tile to have trees on it

    Returns:
        array(string(string)): Representation of the terrain
    """
    terrain = []
    for rowIndex in range(0, rowSize):
        col = []
        for colIndex in range(0, colSize):
            # chooses between the two states with a luck of 1-treeLuck for the first and of treeLuck for the second
            # list states needs to be mandatory of length 2
            col.append(random.choices(
                states, [1-treeLuck, treeLuck])[0])
        terrain.append(col)
    return terrain


def fireCells(terrain):
    """
    Sets Cells on fire

    For the purpose of determining which ones, randomly

    Parameters:
        terrain(list(list(string))): The representation of the terrain

    Returns:
        list(list(string)): The case with the FIREs, ready to be started
    """
    for burnTiles in range(0, random.randrange(0, int((len(terrain)*len(terrain[0]))**1))):
        terrain[random.randrange(0, len(terrain))][random.randrange(
            0, len(terrain[0]))] = "FIRE"
    return terrain


def allInchanged(terrain):
    """
    Checks if all the tiles are inchanged i.e. : Void or Tree or Ash

    Parameters:
        terrain(list(list(string))): The representation of the terrain

    Returns:
        bool: True if all the tiles are inchanged, False else
    """
    all = True
    for rows in terrain:
        for tiles in rows:
            all &= tiles == "Void" or tiles == "Tree" or tiles == "Ash"
    return all


def probaFire(neighborsOnFire):
    """
    Returns the probability to set a tree in fire

    Probability to set on fire: 1-1/(k+1) where k equal the number of neighbors on fire

    Parameters:
        neighborsOnFire(int)

    Returns:
        double
    """
    k = neighborsOnFire
    return 1-1/(k+1)


def evolutionTree(terrain, treeStates, xIndex, yIndex, isFireProba):
    """
    Set the tree on FIRE by following the rules or not

    Probability to set on fire: 1-1/(k+1) where k equals the number of neighbors on fire

    Parameters:
        terrain(list(list(string))): The representation of the terrain
        treeStates(list(string)): The differents states a tree can have when fire is around him
        xIndex(int): the horizontal coordinate of where the tree is
        yIndex(int): the vertical coordinate of where the tree is
        isFireProba(bool): True: The determination of the fire is probabilistic
                           False: The determination of the fire depend solely on its neighbors

    Returns:
        list(list(string)): the representation of the terrain with the tree on fire
    """
    neighborsOnFire = 0
    # iterating for the 4 cardinal points in order to check all the tiles around the one we want
    cards=[[-1,0],[0,-1],[1,0],[0,1]]
    for xyCard in cards:        
        try:           
            if(terrain[xIndex+xyCard[0]][yIndex+xyCard[1]] == "FIRE"):
                neighborsOnFire += 1
        except:            
            pass
    if(isFireProba):
        terrain[xIndex][yIndex] = random.choices(
            treeStates, [probaFire(neighborsOnFire), 1-probaFire(neighborsOnFire)])[0]
    elif(neighborsOnFire > 0):
        terrain[xIndex][yIndex] = "FIRE"
    return terrain

def launch(terrain, treeStates, evolution, isFireProba):
    """
    Launch the simulation of the Forest Fires    

    Parameters:
        terrain(list(list(string))): The representation of the terrain
        treeStates(list(string)): The differents states a tree can have when fire is around him
        evolution(dict(string,string)): States the evolution of each tile after one iteration in the terrain    
        isFireProba(bool): True: The determination of the fire is probabilistic
                           False: The determination of the fire depend solely on its neighbors

    Returns
        list(list(list(string))): All the evolutions of the Terrain
    """
    terrainE=cloneLL(terrain)

    terrainsEvolution = []
    terrainsEvolution.append(cloneLL(terrainE))

    while(not allInchanged(terrainE)):
        for xIndex in range(0, len(terrainE)):
            for yIndex in range(0, len(terrainE[0])):
                if terrainE[xIndex][yIndex] != 'Tree':
                    terrainE[xIndex][yIndex] = evolution[terrainE[xIndex][yIndex]]
                else:
                    terrainE = evolutionTree(
                        cloneLL(terrainE), treeStates, xIndex, yIndex, isFireProba)
        terrainsEvolution.append(cloneLL(terrainE))
    return terrainsEvolution


def automaton(rowSize, colSize, states, treeStates, evolution, treeLuck, isFireProba):
    # "main()"
    """
    Initialization and Launching of the cellular automaton

    Parameters:
        rowSize(int): Size Horizontically 
        colSize(int): Size Vertically
        states(list(string)): The differents states the tile can have when its start
        treeStates(list(string)): The differents states a tree can have when fire is around him
        evolution(dict(string,string)): States the evolution of each tile after one iteration in the terrain
        treeLuck(double): the percentage of luck for a tile to have trees on it
        isFireProba(bool): True: The determination of the fire is probabilistic
                           False: The determination of the fire depend solely on its neighbors
    """
    terrain = init(states, rowSize, colSize, treeLuck)
    terrain = fireCells(terrain)
    terrainsEvolution = launch(terrain, treeStates, evolution, isFireProba)
    for t in terrainsEvolution:
        printTwoDArr(t)


def printTwoDArr(twoDArr):
    """
    Prints a 2D Array

    Skips a line at the end for visibility

    Parameters:
        twoDArr: the 2D Array we are printing
    """
    for elem in twoDArr:
        print(elem)
    print("")


def cloneL(eglist):
    """
    Clones a list
    """
    new = []
    for elem in eglist:
        new.append(elem)
    return new


def cloneLL(eglistlist):
    """
    Clones a list of lists
    """
    new = []
    for eglist in eglistlist:
        new.append(cloneL(eglist))
    return new


if __name__ == "__main__":  
    # "main"
    automaton(rowSize, colSize, initialStates,
              treeStates, evolution, 1-voidLuck, isFireProba)
