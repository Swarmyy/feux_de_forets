"""
==============================================================================
Graphic Interface for Forest Fires
==============================================================================
Author : MONTOUCHET Teddy
"""

from tkinter import Button, Canvas, Tk
from automaton import caseSize, cloneLL, colSize, init, initialStates, launch, rowSize, voidLuck, isFireProba, evolution, treeStates
import time

# dict(string,string): The dictionnary attributing to each state a Color
statesToColor = {
    "Void": "white",
    "Tree": "#476042",
    "FIRE": "orange",
    "Ash": "gray"
}


def forestGeneration(terrain):
    """
    Initialization of the graphical representation of the terrain

    Generating all the canvas according to the terrain initialization

    Parameters:
        terrain(list(string)): Representing the initial states of the tiles        
    """
    for rowIndex in range(0, rowSize):
        for colIndex in range(0, colSize):
            canvas.create_rectangle(colIndex*caseSize+caseSize, rowIndex *
                                    caseSize+caseSize, colIndex*caseSize, rowIndex*caseSize, fill=statesToColor[terrain[rowIndex][colIndex]])


def newMap(initialStates, rowSize, colSize, voidLuck):
    """
    Generation of the terrain and display it

    Parameters:
        initialStates(list(string)): The differents states the tile can have when its start
        rowSize(int): Size Horizontically 
        colSize(int): Size Vertically
        voidLuck(double): the percentage of luck for a tile to have void on it
    """
    canvas.delete("all")
    terrain = init(initialStates, rowSize, colSize, voidLuck)
    forestGeneration(terrain)
    return terrain


def click_callback(event):
    """
    Handles the mouse click to put the trees on fire

    Parameters:
        event(event): The MouseClick event
    """
    x = event.x//caseSize
    y = event.y//caseSize
    if (terrain[y][x] == "Tree"):
        terrain[y][x] = "FIRE"
    forestGeneration(terrain)


def simulation(terrain, frame):
    terrainsEvolution = launch(terrain, treeStates, evolution, isFireProba)
    displayTerrain(terrainsEvolution, frame, len(terrainsEvolution))


def displayTerrain(terrainsEvolution, frame, index):
    if(index > 0):
        forestGeneration(terrainsEvolution[len(terrainsEvolution)-index])
        frame.after(500, lambda: displayTerrain(
            terrainsEvolution, frame, index-1))

def reset_callback(initTerrain,terrain):
    terrain=initTerrain
    forestGeneration(initTerrain)

if __name__ == "__main__":
    """
    Defining the windows's parameters
    """
    frame = Tk()
    frame.title("Feux de forêt")
    frame.minsize(100, 50)
    canvas_width = caseSize*colSize
    canvas_height = caseSize*rowSize
    canvas = Canvas(frame, width=canvas_width, height=canvas_height)
    canvas.pack(anchor='center')

    # at start
    terrain = newMap(initialStates, rowSize, colSize, voidLuck)
    initTerrain = cloneLL(terrain)

    resetButton = Button(frame, text="Reset Map",
                         command=lambda: reset_callback(initTerrain,terrain))
    resetButton.pack()

    RegenerateButton = Button(frame, text="New Map", command=lambda: newMap(
        initialStates, rowSize, colSize, voidLuck))
    RegenerateButton.pack()

    canvas.bind('<Button-1>', click_callback)

    launchButton = Button(frame, text="Launch Simulation",
                          command=lambda: simulation(terrain, frame))
    launchButton.pack()

    frame.mainloop()
